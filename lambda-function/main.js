'use strict'

exports.handler = function (event, context, callback) {
  var response = {
    statusCode: 200,
    headers: {
      'Content-Type': 'text/html; charset=utf-8'
    },
    body: '<script type="text/javascript" src="https://platform.linkedin.com/badges/js/profile.js" async defer></script><h1>This is hello-terraform-serverless.</h1><p><a href="https://gitlab.com/willhallonline/hello-terraform-serverless">Hello Terraform Serverless on GitLab</a><br />Have a great rest of the day. I\'ve am still Will Hall :) </p><div class="LI-profile-badge"  data-version="v1" data-size="large" data-locale="en_US" data-type="vertical" data-theme="dark" data-vanity="willhallonline"><a class="LI-simple-link" href="https://uk.linkedin.com/in/willhallonline?trk=profile-badge">Will Hall</a>. Ive just updated it today.</div>'
  }
  callback(null, response)
}
